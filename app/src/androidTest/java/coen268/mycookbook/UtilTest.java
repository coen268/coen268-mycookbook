package coen268.mycookbook;

import android.content.Context;
import android.test.AndroidTestCase;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * Created by kaushik on 5/27/15.
 */

public class UtilTest extends AndroidTestCase {
    private Context ctx;

    public void setUp() {
        this.ctx = getContext();
    }
    public  void testUtilHtml() {
        Util u = Util.getInstance();
        CookingSession s = new CookingSession(1,1,"Session001");
        Map<String,Object> map = u.getReportData(ctx, s);
        String html = (String)map.get("html");
        assertNotNull(html);
        try {
            u.copyData(map,s.getName());

            FileOutputStream os =ctx.openFileOutput("out-" + System.currentTimeMillis() + ".html", Context.MODE_PRIVATE);
            FileWriter w = new FileWriter(os.getFD());
            w.write(html);
            w.flush();
            w.close();
        } catch (FileNotFoundException e) {
            assertTrue(false) ;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
        System.out.println(html);
    }
}
