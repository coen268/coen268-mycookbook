package coen268.mycookbook.ui;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Map;

import coen268.mycookbook.CookingSession;
import coen268.mycookbook.Dao;
import coen268.mycookbook.R;
import coen268.mycookbook.Recipe;
import coen268.mycookbook.ShareActivity;
import coen268.mycookbook.SharedModel;
import coen268.mycookbook.Util;

public class SessionListActivity extends ListActivity {

    private Dao dao;
    private List<CookingSession> sessions;
    private SessionListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_list);
        this.dao = Dao.getInstance(this);
        long recipeId = getIntent().getLongExtra("recipeId", -1);
        Recipe r=null;
        if(recipeId>0) {
            r = dao.getRecipeById(recipeId);
            sessions = dao.getSessionByRecipeId(r.getId());
            setTitle("Sessions - "+r.getTitle());
        }else {
            sessions = dao.getSessionByRecipeId(recipeId);
        }

        adapter = new SessionListAdapter(this,R.layout.session_list_view,sessions,r);
        setListAdapter(adapter);
        registerForContextMenu(getListView());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //no menu
        return false;
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
         if(v.getId() == getListView().getId()) {
             AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
             getMenuInflater().inflate(R.menu.ctx_menu_session_list,menu);
         }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int pos = info.position;
        CookingSession session = sessions.get(pos);
        if(R.id.ctx_menu_session_preview == item.getItemId()) {
            Toast.makeText(this, "Preview", Toast.LENGTH_SHORT).show();
            Map data = Util.getInstance().getReportData(this, session);
            String url="";
            try {
                url=Util.getInstance().copyData(data, session.getName());
            } catch (Exception e) {
                Toast.makeText(this,"Error generating preview "+item.getTitle(),Toast.LENGTH_SHORT).show();
            }
            WebView view = new WebView(this);
            Uri parse = Uri.parse("file://"+url + "/index.html");
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setDataAndType(parse,"text/html");
            startActivity(i);



        }else if(R.id.ctx_menu_session_fb == item.getItemId()) {
            Intent intent = new Intent(this, ShareActivity.class);
            intent.putExtra("sessionId", session.getSessionId());
            intent.putExtra("SHARE_TYPE","");
            startActivity(intent);
            Toast.makeText(this,"POST TO FB",Toast.LENGTH_SHORT).show();
        }else if(R.id.ctx_menu_session_delete == item.getItemId()) {

            CookingSession s = session;
            int rows = dao.deleteSession(s.getSessionId());
            if(rows>0) {
                Toast.makeText(this,"DELETED "+item.getTitle(),Toast.LENGTH_SHORT).show();
                sessions.remove(pos);
                adapter.notifyDataSetChanged();
            }

        }
        return true;
    }

    private class SessionListAdapter extends ArrayAdapter<CookingSession> {

        private final Recipe recipe;
        private final List<CookingSession> session;
        private final int res;

        public SessionListAdapter(Context context, int resource, List<CookingSession> objects,Recipe r) {
            super(context, resource, objects);
            this.recipe = r;
            this.session = objects;
            this.res = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View layout = getLayoutInflater().inflate(res,parent,false);
            TextView sessionTxt = (TextView) layout.findViewById(R.id.sl_session_name_txt);
            TextView recipeTxt = (TextView) layout.findViewById(R.id.sl_recipe_name_txt);
            sessionTxt.setText(session.get(position).getName());
            if(recipe!=null)
                recipeTxt.setText(recipe.getTitle());
            return layout;
        }
    }

}
