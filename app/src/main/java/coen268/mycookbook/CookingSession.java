package coen268.mycookbook;

import java.util.Date;

/**
 * Created by kaushik on 5/19/15.
 */
public class CookingSession {
    private long sessionId;
    private long parentRecipeId;
    private String name;
    private Date createDate;
    private Date lastUpdated;

    public CookingSession(long sessionId) {
        this.sessionId = sessionId;
    }

    public CookingSession(long sessionId, long parentRecipeId, String name) {
        this.sessionId = sessionId;
        this.parentRecipeId = parentRecipeId;
        this.name = name;
    }

    public CookingSession(long parentRecipeId, String name) {
        this.parentRecipeId = parentRecipeId;
        this.name = name;
    }

    public long getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public long getParentRecipeId() {
        return parentRecipeId;
    }

    public void setParentRecipeId(int parentRecipeId) {
        this.parentRecipeId = parentRecipeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
