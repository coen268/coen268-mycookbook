package coen268.mycookbook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import coen268.mycookbook.ui.RecipeView;

import static coen268.mycookbook.SharedModel.*;
/**
 * Created by Mantu on 20-05-2015.
 */
public class ListofRecipe extends Activity implements AdapterView.OnItemClickListener {


    private Dao dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listofrecipe);
        // Intent intent =getIntent();
        this.dao = Dao.getInstance(this);
        ArrayList<Recipe> recipelist = (ArrayList) dao.getAllRecipes();
        SharedModel.setRecipeList(recipelist);
        ListView listView = (ListView) findViewById(R.id.Recipelist);

        listView.setAdapter(new CustomAdapter(this, R.layout.customlayout, recipelist));
        listView.setOnItemClickListener(this);

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SharedModel.setRecipeIndex(position);
        //TODO - Convert to Fragment
        startActivity(new Intent(this,RecipeView.class));
//        startActivity(new Intent(this,RecipeDescActivity.class));
    }
}
