package coen268.mycookbook;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import java.util.TimerTask;

/**
 * Created by ramyajagannath on 5/22/15.
 */
class CustomTimerTask extends TimerTask {
    private Context mContext;
    private static int mNotificationId = 0;
    CustomTimerTask(Context context){
        mContext = context;
    }

    @Override
    public void run() {

        /*NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle("MyCookBook notification");
        mBuilder.setContentText("Timer alert!");
        mBuilder.setAutoCancel(true);
        mBuilder.setLights(Color.BLUE, 500, 500);
        long[] pattern = {500,500,500,500,500,500,500,500,500};
        mBuilder.setVibrate(pattern);
        mBuilder.setStyle(new NotificationCompat.InboxStyle());
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);
        if(mNotificationId > 100)
            mNotificationId = 0;
        int NotificationId = mNotificationId++;
        NotificationManager mNotifyMgr =
                (NotificationManager)mContext.getSystemService(mContext.NOTIFICATION_SERVICE);
        mNotifyMgr.notify(NotificationId, mBuilder.build());*/

        mContext.startActivity(new Intent(mContext, TimerAlertActivity.class));
    }

}

