package coen268.mycookbook;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by kaushik on 5/14/15.
 */
public class DropboxFileInfoTask extends AsyncTask<Object,Integer, DropboxAPI.DropboxLink> {

    private DropboxListener act;
    private DropboxAPI<AndroidAuthSession> api;
    private Util util;

    public DropboxFileInfoTask(DropboxAPI<AndroidAuthSession> api, DropboxListener a) {
        this.api = api;
        this.act = a;
        this.util = Util.getInstance();
    }

    @Override
    protected DropboxAPI.DropboxLink doInBackground(Object... params){
        String path= (String) params[0];
        String filename = (String) params[1];
        try {
            DropboxAPI.Entry entries = api.metadata(path + "/" + filename, 1, null, true, null);
                DropboxAPI.DropboxLink share = api.share(entries.path);
                return share;
        } catch (DropboxException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(DropboxAPI.DropboxLink share) {
        act.onDropboxResult(share);
    }
}
