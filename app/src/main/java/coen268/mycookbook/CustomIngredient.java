
package coen268.mycookbook;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mantu on 20-05-2015.
 */

public class CustomIngredient extends ArrayAdapter<Ingredient> {
    private final ArrayList<Ingredient> ingredientlist;
    private final Activity context;
    CustomIngredient adapter;
    private final int resourceId;

    static Map<Long, Boolean > ingrCheck;
    public CustomIngredient(Context context, int resource, ArrayList<Ingredient> ingredientlist) {
        super(context, resource, ingredientlist);
        this.ingredientlist = ingredientlist;
        this.context=(Activity)context;
        this.resourceId =resource;
        ingrCheck = new HashMap<>();
        for(Ingredient i: ingredientlist) {
            ingrCheck.put(i.getId(), Boolean.FALSE);
        }
    }

    public void setAdapter(CustomIngredient adapter)
    {
        adapter=adapter;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Ingredient ingredient = ingredientlist.get(position);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(resourceId, parent,false);


        TextView textView = (TextView) row.findViewById(R.id.ingredient);
        textView.setText(ingredient.getName());
        TextView textView1 = (TextView) row.findViewById(R.id.quantity);
        textView1.setText(ingredient.getQuantity());



        final CheckBox Chk = ((CheckBox) row.findViewById(R.id.checkBox));
        if(SharedModel.getSessionId() > 0) {
            Chk.setVisibility(View.VISIBLE);

        }

        Chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Populate the listInfo with check box status
                ingrCheck.put(ingredientlist.get(position).getId(), isChecked);
                int countCheck=0;
                Map<Long, Boolean> ingrCheck = CustomIngredient.getIngrCheck();
                int totalcount= ingrCheck.size();
                for (Long l:ingrCheck.keySet())
                {
                    if(ingrCheck.get(l))
                    {
                        countCheck+=1;
                    }
                }
                Toast.makeText(context, "You have " + countCheck + "/" + totalcount + " Ingredients", Toast.LENGTH_SHORT).show();
            }
        });

        return row;
    }

    public static Map<Long, Boolean> getIngrCheck() {
        return ingrCheck;
    }

    public void setIngrCheck(Map<Long, Boolean> ingrCheck) {
        this.ingrCheck = ingrCheck;
    }
}
