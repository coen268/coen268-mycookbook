package coen268.mycookbook.util;

import android.util.Log;

import java.util.Date;

import coen268.mycookbook.CookingStep;
import coen268.mycookbook.Dao;
import coen268.mycookbook.Ingredient;
import coen268.mycookbook.Recipe;

/**
 * Created by kaushik on 5/20/15.
 */
public class MockRecipes {

    public static void createRecipes(Dao db) {
        Recipe r = new Recipe("BEACH SUNDAL","Quick and Easy Curry. Can be prepared in 5 minutes, especially when short of time");
        r.setLastUpdated(new Date(System.currentTimeMillis()));
        Ingredient i = new Ingredient("Dried green or white peas", "1/4 kg.");
        r.addIngredient(i);
        r.addIngredient(new Ingredient("Finely chopped raw mango" ,"3 tablespoon"));
        r.addIngredient(new Ingredient("Finely chopped coconut", "2 tablespoons"));
        r.addIngredient(new Ingredient("Mustard seeds " ,"1/4 tsp."));
        r.addIngredient(new Ingredient("Green chillies","2"));
        r.addIngredient(new Ingredient("Hing", "a pinch"));
        r.addIngredient(new Ingredient("Oil" ,"2 tsp."));
        r.addIngredient(new Ingredient("Salt" ,  "to taste"));
        r.addIngredient(new Ingredient("Lemon juice","2 tsp. (optional)"));
        r.addCookingStep(new CookingStep("Pressure cook peas (without salt) until soft, 2 whistles would be fine.  Drain the water completely.",10));
        r.addCookingStep(new CookingStep("Heat oil and add mustard seeds",2));
        r.addCookingStep(new CookingStep(" when they crackle add chopped green chillies and hing",1));
        r.addCookingStep(new CookingStep(" Add cooked peas, salt, and stir fry for a while",0));
        r.addCookingStep(new CookingStep(" Turn off the stove and add chopped raw mango and coconut",0));
        r.addCookingStep(new CookingStep(" Add the lemon juice (optional). Serve hot",0));

        Long recipeId= db.createRecipe(r).getId();
        Log.i("ADD RECIPE", "recipeID=" + recipeId);

        // Millets Bisi Bela Bath

        r = new Recipe("MILLETS BISI BELE BATH","Quick and Easy Curry. Can be prepared in 5 minutes, especially when short of time");
        r.setLastUpdated(new Date(System.currentTimeMillis()));
        i = new Ingredient("Kudiravali (Barnyard millet) or any millet" , "1 cup");
        r.addIngredient(i);
        r.addIngredient(new Ingredient("Toor dhal", "3/4 cup"));
        r.addIngredient(new Ingredient("Tamarind - a lemon sized ball OR Homemade tamarind paste" , "1 1/2 tablespoon"));
        r.addIngredient(new Ingredient("Turmeric powder", "1/4 tsp."));
        r.addIngredient(new Ingredient("Sambar powder" , "1/2 tsp."));
        r.addIngredient(new Ingredient("Mixed vegetables(carrot, beans, potatoes, shelled peas)" , "1 1/4 cup"));
                r.addIngredient(new Ingredient("Salt" , "as per taste"));
        r.addIngredient(new Ingredient("Gingelly oil" , "3 tablespoons"));
        r.addIngredient(new Ingredient("Ghee" ," 2 tsp."));
        r.addIngredient(new Ingredient("Ghee fried cashews "," 8"));
        r.addIngredient(new Ingredient("Mustard seeds " , " 1/2 tsp"));
        r.addIngredient(new Ingredient("Curry leaves " , " few"));
        r.addIngredient(new Ingredient("Water " , "5 cups"));
        r.addIngredient(new Ingredient("Cinnamon stick " , " 1"));
                r.addIngredient(new Ingredient("Cloves " , " 4"));
        r.addIngredient(new Ingredient("Coriander seeds(Dhania) " , " 2 tablespoons"));
        r.addIngredient(new Ingredient("Dry red chillies " , " 2"));
        r.addIngredient(new Ingredient("Byadgi chillies " , " 2"));
        r.addIngredient(new Ingredient("Poppy seeds " , " 1/2 tsp."));
        r.addCookingStep(new CookingStep("Wash millets and pressure cook alongwith toor dhal and turmeric powder.Mash well and keep aside",15));
        r.addCookingStep(new CookingStep("Extract juice from tamarind. Keep aside.",2));
        r.addCookingStep(new CookingStep(" In a heavy-bottomed pan, heat a spoon of oil and fry the items mentioned in to powder in order and dry grind in a mixie",10));
        r.addCookingStep(new CookingStep(" In the same pan, heat oil, add the mixed vegetables, little salt and when half done add the tamarind extract, sambar powder and boil until the raw smell goes.",10));
        r.addCookingStep(new CookingStep(" Now add the pressure cooked millet and dhal mixture.",1));
        r.addCookingStep(new CookingStep("  Add salt to suit your taste.",0));

        r.addCookingStep(new CookingStep("  Add a cup of water to get the desired consistency",0));
        r.addCookingStep(new CookingStep("  ACook for a while, in medium heat.Stir frequently until everything is mixed well.",10));
                r.addCookingStep(new CookingStep(" Now add the powdered items. Mix well and stir for a minute",1));
        r.addCookingStep(new CookingStep("  Temper with mustard seeds and curry leaves.",1));
        recipeId= db.createRecipe(r).getId();
        Log.i("ADD RECIPE", "recipeID=" + recipeId);


    // DAHI POORI
        r = new Recipe("DAHI POORI","Dahi puri, a popular chat, is a healthy, cool and easy to prepare snack especially during summer");
                r.setLastUpdated(new Date(System.currentTimeMillis()));

        r.addIngredient(new Ingredient("Mini puris " , " 20"));
        r.addIngredient(new Ingredient("Thick curd " , " 1 1/2 cups"));
        r.addIngredient(new Ingredient("Big Potato  (boiled and mashed)", "1"));
        r.addIngredient(new Ingredient("Chat masala " , " 1 tsp."));
        r.addIngredient(new Ingredient("Mixed vegetables(Red chilli powder " , " 1/2 tsp."));
        r.addIngredient(new Ingredient("Salt " , "to taste"));
        r.addIngredient(new Ingredient("Sev " , " 4 tablespoons"));
        r.addIngredient(new Ingredient("Coriander leaves (finely chopped) " , " 2 tablespoons"));

        r.addCookingStep(new CookingStep("Fry pooris. Beat curd until smooth. A pinch of sugar/salt can be added to the curd if desired",20));
                r.addCookingStep(new CookingStep("Boil potatoes, peel, add little salt and mash roughly. .",10));
        r.addCookingStep(new CookingStep(" Take a poori, crack a hole in the centre and stuff with the potato filling",2));
        r.addCookingStep(new CookingStep("  Add green chutney sweet chutney on top of it",0));
                r.addCookingStep(new CookingStep(" Pour beaten curd on it. Top it with sev and finely chopped coriander leaves.",1));
                        r.addCookingStep(new CookingStep("Sprinkle chat masala and red chilli powder.",1));


       recipeId= db.createRecipe(r).getId();
        Log.i("ADD RECIPE", "recipeID=" + recipeId);

         r = new Recipe("BABY CORN PULAO","Very healthy and colourful one pot meal...");
                r.setLastUpdated(new Date(System.currentTimeMillis()));

        r.addIngredient(new Ingredient("Baby corns " , " 2 cups"));
        r.addIngredient(new Ingredient("Basmati rice " , " 1 cup"));
        r.addIngredient(new Ingredient("Coconut milk " , " 1/4 cup (Thick extract)"));
        r.addIngredient(new Ingredient("Cardamom " , " 1 "));
        r.addIngredient(new Ingredient("Cinnamon " ,  "2 stick ,  2 nos."));
                r.addIngredient(new Ingredient("Black cardamom " , " 2"));
        r.addIngredient(new Ingredient("Cloves " , " 3 or 4"));
        r.addIngredient(new Ingredient("Bay leaves " , "1"));
        r.addIngredient(new Ingredient("Cumin seeds " , " 1/2 tsp."));

        r.addIngredient(new Ingredient("Grated ginger " , " 1/4 tsp."));

        r.addIngredient(new Ingredient("Chilli sauce " , " 1 teaspoon"));

        r.addIngredient(new Ingredient("White pepper powder " , " 1/4 tsp."));

        r.addIngredient(new Ingredient("Salt " , " as per taste"));

        r.addIngredient(new Ingredient("Oil " , " 1 tablespoon"));

        r.addIngredient(new Ingredient("Butter " , " 1 tablespoon"));

        r.addIngredient(new Ingredient("Water + coconut milk " , " 2 cups"));


        r.addCookingStep(new CookingStep("Soak basmati rice in water for half an hour",60));
        r.addCookingStep(new CookingStep("Cut baby corn into 2 long pieces, add a cup of water, pressure cook alongwith salt and turmeric powder for 2 whistles, drain the water and keep aside.",20));
        r.addCookingStep(new CookingStep(" In a pressure pan, heat oil and butter. Add all the whole spices and saute for a minute.",0));

        r.addCookingStep(new CookingStep("  Add green chutney, sweet chutney on top of it",0));
                r.addCookingStep(new CookingStep(" Add Cumin Seeds and when they sizzle add grated ginger, cooked baby corn and chilli sauce. Saute for another minute.",0));

        r.addCookingStep(new CookingStep("Add coconut milk and water.",0));

        r.addCookingStep(new CookingStep("Drain the water completely from the rice. When the water boils add the rice into it.",5));



        r.addCookingStep(new CookingStep(" Mix well, cover and pressure cook for 3 whistles. When the pressure is released, take out, add white pepper powder",15));

        r.addCookingStep(new CookingStep("Give a gentle stir without breaking the grains",3));

        r.addCookingStep(new CookingStep("Transfer it to a bowl and serve hot with masala papads or any raitha of your choice",1));
        recipeId= db.createRecipe(r).getId();
        Log.i("ADD RECIPE", "recipeID=" + recipeId);


        r = new Recipe("Cucumber Stir Fry","Quick and Easy Curry. Can be prepared in 5 minutes, especially when short of time");
        r.setLastUpdated(new Date(System.currentTimeMillis()));
        //add ingredients
        i = new Ingredient("Organic Cucumber","1 lb");
        r.addIngredient(i);
        r.addIngredient(new Ingredient("Sambhar Powder", "1 tsp"));
        r.addIngredient(new Ingredient("Cooking Oil", "2 tsp"));
        r.addIngredient(new Ingredient("Cumin Seeds", "1/4 tsp"));
        r.addIngredient(new Ingredient("Split Urad Dal", "1 tsp"));
        r.addIngredient(new Ingredient("Turmeric Powder", "1/4 tsp"));
        r.addIngredient(new Ingredient("Salt", "As per taste"));
        //add steps
        CookingStep step = new CookingStep("Chop cucumber to small pieces", 5);
        r.addCookingStep(step);
        r.addCookingStep(new CookingStep("Heat Oil in a heavy bottomed pan",10));
        r.addCookingStep(new CookingStep("Add Cumin Seeds, Split Urad Dal and saute",1));
        r.addCookingStep(new CookingStep("Add turmeric powder",1));
        r.addCookingStep(new CookingStep("Add Cucumber, Sambhar Powder and Salt",1));
        r.addCookingStep(new CookingStep("Saute",1));
        r.addCookingStep(new CookingStep("Close the pan and let it cook for 5 minutes",5));
        recipeId= db.createRecipe(r).getId();
        Log.i("ADD RECIPE", "recipeID=" + recipeId);

    }

  }
